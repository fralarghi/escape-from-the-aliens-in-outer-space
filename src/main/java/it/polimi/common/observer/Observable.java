package it.polimi.common.observer;

public interface Observable {
    void register(Observer obs);
}
