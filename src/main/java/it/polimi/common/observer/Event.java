package it.polimi.common.observer;

public final class Event {
    private final String msg;

    public Event(String msg) {
        this.msg = msg + "\n";
    }

    public String getMsg() {
        return msg;
    }
}
