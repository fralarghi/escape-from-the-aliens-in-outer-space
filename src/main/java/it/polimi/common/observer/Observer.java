package it.polimi.common.observer;

public interface Observer {
    void notify(BaseObservable source, Event event);

	void print(Event event);

}
