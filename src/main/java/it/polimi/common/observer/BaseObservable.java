package it.polimi.common.observer;

import java.util.HashSet;
import java.util.Set;

public abstract class BaseObservable {

    private static Set<Observer> observers;
    public static Observer currentObserver;

    public BaseObservable() {
        BaseObservable.observers = new HashSet<Observer>();
        
    }

    public static void register(Observer obs) {
        observers.add(obs);
    }
    
    public static void observing(Observer obs){
    	currentObserver=obs;
    }

    protected final void notifyAll(Event event) {
        for(Observer obs : observers){
            obs.notify(this, event);
        }
    }
    
    protected final void notify(Event event) {
    	 currentObserver.print(event);
        }
}

