package it.polimi.view;


import javax.swing.*;
import javax.sound.sampled.*;


import java.io.*;
@SuppressWarnings("serial")
public class Sounds extends JFrame{
	
	static File noise;
	static File humanKilled;
	static File adrenaline;
	static File sedatives;
	static File teleport;
	static File attack;
	static File attackActivated;
	static File lights;
	static File alienKilled;
	
	public Sounds() {
		File gameMusic =new File("./src/main/resources/Atmo.wav");
		noise =new File("./src/main/resources/Noise.wav");
		humanKilled =new File("./src/main/resources/HumanKilled.wav");
		adrenaline =new File("./src/main/resources/Adrenaline.wav");
		sedatives =new File("./src/main/resources/Sedatives.wav");
		teleport =new File("./src/main/resources/Teleport.wav");
		attack =new File("./src/main/resources/Attack.wav");
		attackActivated =new File("./src/main/resources/AttackActivated.wav");
		lights =new File("./src/main/resources/Lights.wav");
		alienKilled =new File("./src/main/resources/AlienKilled.wav");
		
		try {
			
			final Clip clip = AudioSystem.getClip();
			final AudioInputStream inputStream = AudioSystem.getAudioInputStream(gameMusic);
			clip.open(inputStream);
			clip.loop(Clip.LOOP_CONTINUOUSLY);

		} catch (FileNotFoundException ex) {
			
			ex.printStackTrace();
		} catch (LineUnavailableException line) {
			
			line.printStackTrace();
		} catch (UnsupportedAudioFileException uns) {
			
			uns.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	public static void noise(){
		try{
			final Clip clip = AudioSystem.getClip();
			final AudioInputStream inputStream = AudioSystem.getAudioInputStream(noise);
			clip.open(inputStream);
			clip.start();
		} catch (FileNotFoundException ex) {
			
			ex.printStackTrace();
		} catch (LineUnavailableException line) {
			
			line.printStackTrace();
		} catch (UnsupportedAudioFileException uns) {
			
			uns.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	public static void humanKilled(){
		try{
			final Clip clip = AudioSystem.getClip();
			final AudioInputStream inputStream = AudioSystem.getAudioInputStream(humanKilled);
			clip.open(inputStream);
			clip.start();
		} catch (FileNotFoundException ex) {
			
			ex.printStackTrace();
		} catch (LineUnavailableException line) {
			
			line.printStackTrace();
		} catch (UnsupportedAudioFileException uns) {
			
			uns.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	public static void adrenaline(){
		try{
			final Clip clip = AudioSystem.getClip();
			final AudioInputStream inputStream = AudioSystem.getAudioInputStream(adrenaline);
			clip.open(inputStream);
			clip.start();
		} catch (FileNotFoundException ex) {
			
			ex.printStackTrace();
		} catch (LineUnavailableException line) {
			
			line.printStackTrace();
		} catch (UnsupportedAudioFileException uns) {
			
			uns.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	public static void sedatives(){
		try{
			final Clip clip = AudioSystem.getClip();
			final AudioInputStream inputStream = AudioSystem.getAudioInputStream(sedatives);
			clip.open(inputStream);
			clip.start();
		} catch (FileNotFoundException ex) {
			
			ex.printStackTrace();
		} catch (LineUnavailableException line) {
			
			line.printStackTrace();
		} catch (UnsupportedAudioFileException uns) {
			
			uns.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	public static void teleport(){
		try{
			final Clip clip = AudioSystem.getClip();
			final AudioInputStream inputStream = AudioSystem.getAudioInputStream(teleport);
			clip.open(inputStream);
			clip.start();
		} catch (FileNotFoundException ex) {
			
			ex.printStackTrace();
		} catch (LineUnavailableException line) {
			
			line.printStackTrace();
		} catch (UnsupportedAudioFileException uns) {
			
			uns.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	public static void lights(){
		try{
			final Clip clip = AudioSystem.getClip();
			final AudioInputStream inputStream = AudioSystem.getAudioInputStream(lights);
			clip.open(inputStream);
			clip.start();
		} catch (FileNotFoundException ex) {
			
			ex.printStackTrace();
		} catch (LineUnavailableException line) {
			
			line.printStackTrace();
		} catch (UnsupportedAudioFileException uns) {
			
			uns.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	public static void attack(){
		try{
			final Clip clip = AudioSystem.getClip();
			final AudioInputStream inputStream = AudioSystem.getAudioInputStream(attack);
			clip.open(inputStream);
			clip.start();
		} catch (FileNotFoundException ex) {
			
			ex.printStackTrace();
		} catch (LineUnavailableException line) {
			
			line.printStackTrace();
		} catch (UnsupportedAudioFileException uns) {
			
			uns.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	public static void attackActivated(){
		try{
			final Clip clip = AudioSystem.getClip();
			final AudioInputStream inputStream = AudioSystem.getAudioInputStream(attackActivated);
			clip.open(inputStream);
			clip.start();
		} catch (FileNotFoundException ex) {
			
			ex.printStackTrace();
		} catch (LineUnavailableException line) {
			
			line.printStackTrace();
		} catch (UnsupportedAudioFileException uns) {
			
			uns.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

	public static void alienKilled() {
		try{
			final Clip clip = AudioSystem.getClip();
			final AudioInputStream inputStream = AudioSystem.getAudioInputStream(alienKilled);
			clip.open(inputStream);
			clip.start();
		} catch (FileNotFoundException ex) {
			
			ex.printStackTrace();
		} catch (LineUnavailableException line) {
			
			line.printStackTrace();
		} catch (UnsupportedAudioFileException uns) {
			
			uns.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	
}