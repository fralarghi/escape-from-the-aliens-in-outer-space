package it.polimi.view;

import it.polimi.controller.Controller;
import it.polimi.model.Board;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class OptionsScreen {
	
	JLabel game;
	String[] numPlayers = {"","4","5","6","7","8"};
	JLayeredPane layeredPane;
	@SuppressWarnings("rawtypes")
	JComboBox boxPlayers;
	JLabel playersLabel;
	JButton play;
	String[] colorsBox = {"","Blue","Brown","Green","Orange","Pink","Purple","Red","Yellow"};
	@SuppressWarnings("rawtypes")
	JComboBox color;
	JTextField name;
	CopyOnWriteArrayList<JTextField> names;
	@SuppressWarnings("rawtypes")
	CopyOnWriteArrayList<JComboBox> colors;
	int players;
	JFrame screen;
	
	public OptionsScreen(final JFrame options, final JFrame screen){
		this.screen=screen;
		options.setVisible(true);
		options.setSize(400,290);
		options.setLocationRelativeTo(null);
		options.setResizable(false);
		options.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		verifyFrameClosing(screen,options);
		options.setTitle("Settings");
		layeredPane = new JLayeredPane();
		options.getContentPane().add(layeredPane);
		layeredPane.setPreferredSize(new Dimension(400,290));
		addBoxPlayers();
		addPlayButton(options);
		
		options.setSize(399,289);
		options.setSize(400,290);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addBoxPlayers(){
		boxPlayers = new JComboBox(numPlayers);
		playersLabel = new JLabel("Number of players:");
		layeredPane.add(playersLabel);
		layeredPane.add(boxPlayers);
		boxPlayers.setBounds(50, 45, 50, 20);
		playersLabel.setBounds(20, 20, 150, 20);
		boxPlayers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox box = (JComboBox)e.getSource();
				String num = (String)box.getSelectedItem();
				for(String i : numPlayers){
					if(i.equals(num) && !(i.equals(""))){
						
						refreshPane();
						refreshLayeredPane();
						
						players = Integer.parseInt(num);
						int offset = 0;
						
						addLabels();
						addColorBoxes(players,offset);
						offset=0;
						addNameTextAreas(players,offset);
						
					}
					if(i.equals("")){
						refreshPane();
					}
				}
			}
		});
	}
	
	public void addPlayButton(final JFrame options){
		play = new JButton("Play!");
		layeredPane.add(play);
		play.setBounds(20,100,100,20);
		play.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(verifyColors()==false || verifyNames()==false){
					JOptionPane.showMessageDialog(layeredPane, "Not ready yet!");
				}
				else{
					screen.setEnabled(true);
					WindowEvent close = new WindowEvent(options, WindowEvent.WINDOW_CLOSING);
					options.dispatchEvent(close);
					refreshScreen();
					game = new JLabel(new ImageIcon("./src/main/resources/Space.jpg")); 
					screen.setContentPane(game);
					
					Board board = new Board(names.size());
					Controller controller = new Controller(board);
					
					GameScreen gameScreen = new GameScreen(screen,names,colors,controller);
					
					try{
						gameScreen.start();
					}
				    catch(IOException io){
				        System.err.println("Error from Input-Output!");
				    } 
				    catch (Exception ex) {
				        System.err.println("Generic error!");
					}
				}
			}
		});
	}

	public void refreshLayeredPane(){
		layeredPane.setSize(399,289);
		layeredPane.setSize(400,290);
	}
	
	public void refreshScreen(){
		screen.setSize(1099,749);
		screen.setSize(1100,750);
	}
	
	public void refreshPane(){
		if(layeredPane.getComponents().length>3){
			for(int m=0;m<5;m++){
				for(int l=4;l<layeredPane.getComponents().length;l++){
					layeredPane.remove(layeredPane.getComponents()[l]);
					layeredPane.setSize(399,289);
					layeredPane.setSize(400,290);
				}
			}
		}
	}
	
	public void addLabels(){
		JLabel playerNameLabel = new JLabel("Names:");
		JLabel playerColorLabel = new JLabel("Colors:");
		layeredPane.add(playerNameLabel);
		layeredPane.add(playerColorLabel);
		playerColorLabel.setBounds(270,20,150,20);
		playerNameLabel.setBounds(150,20,150,20);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addColorBoxes(int players, int offset){
		colors = new CopyOnWriteArrayList<JComboBox>();
		for(int iterator=1;iterator<=players;iterator++){
			color = new JComboBox(colorsBox);
			layeredPane.add(color);
			color.setBounds(270,45+offset,80,20);
			offset=offset+25;
			colors.add(color);
		}
	}
	
	public void addNameTextAreas(int players, int offset){
		names = new CopyOnWriteArrayList<JTextField>();
		for(int iterator=1;iterator<=players;iterator++){
			name = new JTextField();
			layeredPane.add(name);
			name.setBounds(150,45+offset,100,20);
			name.setEditable(true);
			offset=offset+25;
			names.add(name);
		}
	}
	
	public boolean verifyNames(){
		
		for(JTextField name : names){
			if(name.getText().equals("")) {
				return false;
			}
		}
		return true;
	}
	
	public boolean verifyColors(){
		
		for(@SuppressWarnings("rawtypes") JComboBox color : colors){
			if(color.getSelectedIndex()==0){
				return false;
			}
		}
		return true;
	}
	
	public void verifyFrameClosing(final JFrame screen, final JFrame options){
		options.addWindowListener(new WindowAdapter() {
			public void windowClosing (WindowEvent e) {
				options.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				screen.setEnabled(true);
			}
		});
	}
}
