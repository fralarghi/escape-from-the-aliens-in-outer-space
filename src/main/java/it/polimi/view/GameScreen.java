package it.polimi.view;

import java.awt.*;
import java.util.List;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.*;

import it.polimi.controller.Controller;
import it.polimi.model.Player;
import it.polimi.model.Position;
import it.polimi.model.exceptions.*;


public class GameScreen {
	
	public static final int SPRITE_WIDTH = 41;
	public static final int SPRITE_HEIGHT = 36;
	
	JLayeredPane layeredPane;
	Console console;
	JFrame screen;
	Controller controller;
	Client currentClient;
	public ArrayList<Client> clients;
	Iterator<Client> iterClients;
	CopyOnWriteArrayList<JTextField> names;
	
	@SuppressWarnings("rawtypes")
	CopyOnWriteArrayList<JComboBox> colors;
	JLabel player;
	CopyOnWriteArrayList<JLabel> sectorsToDraw;
	
	GameButton activateButton;
	GameButton attackButton;
	GameButton passButton;
	GameButton startButton;
	
	int numTurn;
	
	private boolean moving;
	private boolean waitingPosition;
	protected boolean lightsActivated;
	protected CardScreen cardScreen;
	GameScreen gameScreen;
	
	public GameScreen(JFrame screen, CopyOnWriteArrayList<JTextField> names, @SuppressWarnings("rawtypes") CopyOnWriteArrayList<JComboBox> colors, Controller controller) {
		this.controller = controller;
		this.names = names;
		this.colors = colors;
		this.screen = screen;
		this.clients = new ArrayList<Client>(0);
		moving=false;
		waitingPosition=false;
		lightsActivated=false;
		sectorsToDraw = new CopyOnWriteArrayList<JLabel>();
	}
	
	
	public void start() throws Exception{
		screen.setSize(1099,749);
		screen.setSize(1100,750);
		layeredPane = new JLayeredPane();
		screen.getContentPane().add(layeredPane);
		layeredPane.setPreferredSize(new Dimension(1100,750));
		
		
		console = new Console(layeredPane);
		inizializeClients();
		
		drawTitleMap();
		drawTitleGame();
		
		drawButtons();
		drawGameButtons();
		drawGalilei();
		
		refreshPane();
		
		startGame();
	}
	
	private void refreshPane(){
		layeredPane.setSize(1099,749);
		layeredPane.setSize(1100,750);
	}

	private void startGame() {
		numTurn = 1;
		iterClients = clients.iterator();
		currentClient = iterClients.next();
		Player.observing(currentClient);
		startButton.activation();
	}


	public void createSector(JLabel sector, int x, int y, final Position pos){
	    layeredPane.add(sector,new Integer(1));
		sector.setBounds(x, y, 41, 36);
		sector.setOpaque(false);
		MouseListener mapListener = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(isInside(e) && !controller.sectorIn(pos).equalsIgnoreCase("VOID")){
					
					if(waitingPosition){
						Position target = pos;
				        controller.notifyNoise(target);
				        controller.notifyYou("You have made noise in "+pos);
				        waitingPosition=false;
				        Sounds.noise();
				        
				        refreshAll();
				        
					}
					if(moving && !lightsActivated){
						try{
							moving=false;
							controller.move(pos);
							
							refreshAll();
						
						} catch (BadPositionException bad){
							controller.notifyYou(bad.getMessage());
							moving = true;
							
						} catch (InsertPositionException ins){
							controller.notifyYou("Click where you want to make noise!!");
							waitingPosition=true;
		                
						} catch(RedLifeboatException red) {
							JLabel lifeboat = new JLabel(new ImageIcon("./src/main/resources/LifeboatSectorRed.png"));
							layeredPane.add(lifeboat,new Integer(3));
							lifeboat.setBounds(convertX(pos), convertY(pos), 41, 36);
							lifeboat.setOpaque(false);
							refreshAll();
						
						} catch(GreenLifeboatException green) {
							JLabel lifeboat = new JLabel(new ImageIcon("./src/main/resources/LifeboatSectorGreen.png"));
							layeredPane.add(lifeboat,new Integer(3));
							lifeboat.setBounds(convertX(pos), convertY(pos), 41, 36);
							lifeboat.setOpaque(false);
							passButton.activation();
							
						} catch (TooManyCardsException too){
							controller.notifyYou(too.getMessage());
							refreshAll();
							passButton.disactivation();
							attackButton.disactivation();
							
						}catch(WinException win) {
							endGame(win.getMessage());
							
						}catch (Exception exx){
							controller.notifyYou("Problem with your move!!");
						}
						
					}
					if(lightsActivated){
						lightsActivated = false;
						controller.activateLights(pos);
						eraseValidMoves();
						refreshPane();
						verifyObjectCards();
						
					}
						
				}
			}
		};
	
		sector.addMouseListener(mapListener);
	}

	
	public void setLightsActivated(){
		lightsActivated=true;
		controller.notifyYou("Click where you want to turn on the lights!");
	}
	
	public JLabel voidSector(){
		JLabel voidSector = new JLabel(new ImageIcon("./src/main/resources/VoidSector.png"));
		return voidSector;
	}
	
	public JLabel safeSector(Position secPos){
		JLabel safeSector;
		safeSector = new JLabel(new ImageIcon("./src/main/resources/SafeSector.png"));
		for(Position validMove: controller.validMoves()){
			if(validMove.equals(secPos)){
				safeSector.setIcon(new ImageIcon("./src/main/resources/SafeSector2.png"));
			}
		}
		return safeSector;
	}
	
	public JLabel dangerousSector(Position secPos){
		JLabel dangerousSector;
		dangerousSector = new JLabel(new ImageIcon("./src/main/resources/DangerousSector.png"));
		for(Position validMove: controller.validMoves()){
			if(validMove.equals(secPos)){
				dangerousSector.setIcon(new ImageIcon("./src/main/resources/DangerousSector2.png"));
			}
		}
		return dangerousSector;
	}
	
	public JLabel lifeboatSector(){
		JLabel lifeboatSector = new JLabel(new ImageIcon("./src/main/resources/LifeboatSector.png"));
		
		return lifeboatSector;
	}
	
	public JLabel humanSector(){
		JLabel humanSector = new JLabel(new ImageIcon("./src/main/resources/HumanSector.png"));
		return humanSector;
	}
	
	public JLabel alienSector(){
		JLabel alienSector = new JLabel(new ImageIcon("./src/main/resources/AlienSector.png"));
		return alienSector;
	}
	
	public void drawButtons(){
		gameScreen=this;
		JButton newGame = new JButton("New Game");
		JButton discard = new JButton("Discard");
		JButton exitGame = new JButton("Exit Game");
		layeredPane.add(newGame);
		layeredPane.add(discard);
		layeredPane.add(exitGame);
		discard.setBounds(40, 600, 150, 25);
		exitGame.setBounds(40, 630, 150, 25);
		newGame.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		discard.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		exitGame.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		discard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame cards = new JFrame();
				screen.setEnabled(false);
				cardScreen = new CardScreen(cards, gameScreen, true);
			}
		});
		
		exitGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] dialogButtons = {"Yes","No"};
				int reply = JOptionPane.showOptionDialog(layeredPane, "Are you sure?", "Quit Game", 0, JOptionPane.QUESTION_MESSAGE, null, dialogButtons, dialogButtons[1]);
				if(reply==0){
					System.exit(0);
				}
			}
		});
		
	}
	
	public void drawGameButtons(){
		activateButton = new ActivateButton(layeredPane, controller, this);
		attackButton = new AttackButton(layeredPane, controller, this);
		passButton = new PassButton(layeredPane, controller, this);
		startButton = new StartButton(layeredPane, controller, this);
	}
	
	public void drawTitleMap(){
		JLabel title = new JLabel("  GALILEI MAP");
		layeredPane.add(title);
		title.setFont(new Font("Arial",Font.BOLD,16));
		title.setBounds(350, 15, 120, 20);
		title.setBorder(BorderFactory.createLineBorder(Color.black));
		title.setBackground(new Color(255,255,255,80));
		title.setForeground(Color.black);
		title.setOpaque(true);
	}
	
	public void drawTitleGame(){
		JLabel titleGame = new JLabel(new ImageIcon("./src/main/resources/Title.jpg"));
		layeredPane.add(titleGame, new Integer(0));
		titleGame.setBounds(800, 570, 250, 120);
		titleGame.setBorder(BorderFactory.createLineBorder(Color.black));
		titleGame.setOpaque(false);
	}
	
	public void drawGalilei(){
		
		
		
		String fileLine = new String();
		String fileSector = new String();
		int xSector=40;
		int ySector=40;
		int xText=49;
		int yText=51;
		JLabel pos;
		Integer numLifeboat=1;
		boolean even = false;
		
		try{			
			BufferedReader reader = new BufferedReader(new FileReader("./src/main/java/it/polimi/model/Galilei.txt"));
			for(int i=0; i<322; i++){
				
				fileLine = reader.readLine();
				String filePosition = fileLine.substring(0,4);
				fileSector = fileLine.substring(5);
				Position secPos = sectorPosition(filePosition);
				
				switch (fileSector){
				case "DANGEROUS": createSector(dangerousSector(secPos),xSector,ySector,secPos); break;
				case "SAFE": createSector(safeSector(secPos),xSector,ySector,secPos); break;
				case "VOID": createSector(voidSector(),xSector,ySector,secPos); break;
				case "LIFEBOAT": createSector(lifeboatSector(),xSector,ySector,secPos); break;
				case "HUMAN": createSector(humanSector(),xSector,ySector,secPos); break;
				case "ALIEN": createSector(alienSector(),xSector,ySector,secPos); break;
				}
				
				if(fileSector.equals("DANGEROUS") || fileSector.equals("SAFE")){
					pos = new JLabel(filePosition);
					layeredPane.add(pos,new Integer(4));
					pos.setFont(new Font("Arial",Font.BOLD,10));
					pos.setBounds(xText, yText, 30, 10);
					pos.setOpaque(false);
				}
				else if(fileSector.equals("LIFEBOAT")){
					pos = new JLabel(numLifeboat.toString());
					layeredPane.add(pos,new Integer(4));
					pos.setFont(new Font("Arial",Font.BOLD,12));
					pos.setBounds(xText+7, yText, 30, 10);
					pos.setForeground(Color.blue);
					pos.setOpaque(false);
					numLifeboat++;
				}
			
				//updating coordinates
				
				yText=yText+SPRITE_HEIGHT;
				ySector=ySector+SPRITE_HEIGHT;
				
				//changing column
				
				if(i%14==13){
					xText=xText+(3*SPRITE_WIDTH/4);
					xSector=xSector+(3*SPRITE_WIDTH/4);

					if(even==false){
						yText=51+SPRITE_HEIGHT/2;
						ySector=40+SPRITE_HEIGHT/2;
					}
					else {
						yText=51;
						ySector=40;
					}
					even=!even;
				}
				
			}
			reader.close();	
						
		}
		catch(Exception e){
			System.err.println("Problema con il disegno della mappa");
		}
	}
	
	public void removeGalilei(){
		int numComponents = layeredPane.getComponentCount();
		for(int i=0; i<322; i++){
			layeredPane.remove(layeredPane.getComponent(numComponents-i));
		}
	}
	
	public boolean isInside(MouseEvent e){
		int x = e.getX();
		int y = e.getY();
		if(x<(GameScreen.SPRITE_WIDTH-1)/4 && 
				(y<2*x-1+(GameScreen.SPRITE_HEIGHT/2) && 
				y>-2*x+1+(GameScreen.SPRITE_HEIGHT/2)) ||
				(x>(GameScreen.SPRITE_WIDTH-1)/4) &&
				(x<(GameScreen.SPRITE_WIDTH-1)*3/4) ||
				(x>(GameScreen.SPRITE_WIDTH-1)*3/4 && 
				(y<-2*x-1+((GameScreen.SPRITE_HEIGHT/2)+2*GameScreen.SPRITE_WIDTH) && 
				y>2*x+1+((GameScreen.SPRITE_HEIGHT/2)-2*GameScreen.SPRITE_WIDTH)))) {
					return true;
				}
		else {
			return false;
		}
	}
	
	public Position sectorPosition(String filePosition){
		char colPos = filePosition.charAt(0);
		int rowPos = Integer.parseInt(filePosition.substring(2,4));
		return new Position(colPos,rowPos); 
	}

	
	protected void inizializeClients() {
		for(int i=0; i<names.size(); i++){
			Client client = new Client(names.get(i).getText(),colors.get(i).getSelectedItem(),console);
			clients.add(client);
			Player.register(client);
		}
	}
	
	public void startClicked() {
		verifyObjectCards();
		moving = true;
		drawPlayer();
		drawValidMoves();
	}
	
	private void drawPlayer() {
		
		Position pos = controller.getPlayerPos();
		if(controller.getType().equalsIgnoreCase("alien")){
			switch (currentClient.getColor().toUpperCase()){
			case "BLUE": 
				player = new JLabel(new ImageIcon("./src/main/resources/AlienBlue.png"));
				break;
			case "BROWN": 
				player = new JLabel(new ImageIcon("./src/main/resources/AlienBrown.png"));
				break;
			case "GREEN": 
				player = new JLabel(new ImageIcon("./src/main/resources/AlienGreen.png"));
				break;
			case "ORANGE": 
				player = new JLabel(new ImageIcon("./src/main/resources/AlienOrange.png"));
				break;
			case "PINK": 
				player = new JLabel(new ImageIcon("./src/main/resources/AlienPink.png"));
				break;
			case "PURPLE": 
				player = new JLabel(new ImageIcon("./src/main/resources/AlienPurple.png"));
				break;
			case "RED": 
				player = new JLabel(new ImageIcon("./src/main/resources/AlienRed.png"));
				break;
			case "YELLOW": 
				player = new JLabel(new ImageIcon("./src/main/resources/AlienYellow.png"));
				break;
			}
			
		}
		else{
			switch (currentClient.getColor().toUpperCase()){
			case "BLUE": 
				player = new JLabel(new ImageIcon("./src/main/resources/HumanBlue.png"));
				break;
			case "BROWN": 
				player = new JLabel(new ImageIcon("./src/main/resources/HumanBrown.png"));
				break;
			case "GREEN": 
				player = new JLabel(new ImageIcon("./src/main/resources/HumanGreen.png"));
				break;
			case "ORANGE": 
				player = new JLabel(new ImageIcon("./src/main/resources/HumanOrange.png"));
				break;
			case "PINK": 
				player = new JLabel(new ImageIcon("./src/main/resources/HumanPink.png"));
				break;
			case "PURPLE": 
				player = new JLabel(new ImageIcon("./src/main/resources/HumanPurple.png"));
				break;
			case "RED": 
				player = new JLabel(new ImageIcon("./src/main/resources/HumanRed.png"));
				break;
			case "YELLOW": 
				player = new JLabel(new ImageIcon("./src/main/resources/HumanYellow.png"));
				break;
			}
			
		}
		layeredPane.add(player,new Integer(5));
		player.setBounds(convertX(pos), convertY(pos), 41, 36);
		player.setOpaque(false);
		
	}
	
	private void drawValidMoves(){
		for(Position pos: controller.validMoves()){
			JLabel sector;
			String sectorString = controller.sectorIn(pos);
			switch (sectorString){
			case "DANGEROUS":sector = new JLabel(new ImageIcon("./src/main/resources/DangerousSector2.png"));break;
			case "LIFEBOAT":sector = new JLabel(new ImageIcon("./src/main/resources/LifeboatSector2.png"));break;
			default :sector = new JLabel(new ImageIcon("./src/main/resources/SafeSector2.png"));break;
			}
			sectorsToDraw.add(sector);
			layeredPane.add(sector,new Integer(2));
			sector.setBounds(convertX(pos), convertY(pos), 41, 36);
			sector.setOpaque(false);
		}
	}
	
	private void eraseValidMoves(){
		for(JLabel label: sectorsToDraw){
			layeredPane.remove(label);
		}
	}
	
	private void movePlayer(){
		Position pos = controller.getPlayerPos();
		player.setBounds(convertX(pos), convertY(pos), 41, 36);
		player.setOpaque(false);
	}
	
	private void removePlayer(){
		layeredPane.remove(player);
		layeredPane.setSize(1099,749);
		layeredPane.setSize(1100,750);
	}
	
	public int convertX(Position pos){
		return ((int)pos.getCol()-65)*(3*SPRITE_WIDTH/4) + 40;
	}
	
	public int convertY(Position pos){
		if(((int)(pos.getCol()))%2==0){
			return (((pos.getRow()-1)*SPRITE_HEIGHT)+40 + (SPRITE_HEIGHT/2));
		}
		else return (((pos.getRow()-1)*SPRITE_HEIGHT)+40);
	}


	public void verifyObjectCards(){
		if(controller.canActivate()){
			if(controller.cardExist()){
				activateButton.activation();
			}	
		}
	}
	
	public void endTurn() {
		activateButton.disactivation();
		attackButton.disactivation();
		
		removePlayer();
		
		do{
			controller.nextPlayer();
			if(iterClients.hasNext()){
				currentClient = iterClients.next();
			}
			else{
				iterClients = clients.iterator();
				currentClient = iterClients.next();
				numTurn++;
			}
		}while(!controller.isAlive());
		
		if(numTurn>39){
			endGame("You have ended the 39th turn!");
		}
		
		Player.observing(currentClient);
		
		startButton.activation();
	}


	public void cardActivated() {
		if(moving){
			controller.generateMoves();
		}
		controller.printInfo();
		refreshAll();
		drawValidMoves();
	}
	
	public void refreshAll(){
		eraseValidMoves();
		movePlayer();
		refreshPane();
		verifyObjectCards();
		if(controller.attackActivated()){
			attackButton.activation();
		}
		passButton.activation();
	}


	public void endGame(String endCondition) {
		
	    List<String> results = controller.getResult();
	    JFrame scores = new JFrame();
		screen.setEnabled(false);
		
		new ScoresScreen(scores, results, clients, endCondition);
	    
	}

}
	

	

