package it.polimi.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;


@SuppressWarnings("serial")
public class HomeScreen extends JComponent {
	
	JButton startButton;
	JButton gameRulesButton;
	JButton exitButton;
	
	JLabel home;
	JLayeredPane layeredPane;

	public HomeScreen(final JFrame screen){
		
		this.setOpaque(false);
		screen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		home = new JLabel(new ImageIcon("./src/main/resources/HomeScreen.jpg"));
		screen.setContentPane(home);
		layeredPane = new JLayeredPane();
		screen.getContentPane().add(layeredPane);
		
		layeredPane.setPreferredSize(new Dimension(1100,750));
		home.setBounds(0, 0, 1100, 750);
		
		addAuthors();
		drawButtonsHome(screen);
		
		layeredPane.setSize(1099,749);
		layeredPane.setSize(1100,750);
	}
		
	public void addAuthors(){
		JLabel authors = new JLabel("Created by Filippo Maesani & Francesco Larghi");
		layeredPane.add(authors);
		authors.setBounds(5, 5, 500, 16);
		authors.setForeground(Color.white);
		authors.setFont(new Font("Arial",Font.ITALIC,14));
		
		JLabel credits = new JLabel("Music created by Francesco Larghi");
		layeredPane.add(credits);
		credits.setBounds(5, 25, 500, 16);
		credits.setForeground(Color.white);
		credits.setFont(new Font("Arial",Font.ITALIC,14));
		
		JLabel info = new JLabel("We suggest you to play the game with AUDIO ON!");
		layeredPane.add(info);
		info.setBounds(5, 45, 500, 16);
		info.setForeground(Color.white);
		info.setFont(new Font("Arial",Font.BOLD,14));
	}
	
	public void drawButtonsHome(final JFrame screen){
		startButton = new JButton("New Local Game");
		gameRulesButton = new JButton("Game Rules");
		exitButton = new JButton("Exit Game");
		
		
		layeredPane.add(startButton);
		layeredPane.add(gameRulesButton);
		layeredPane.add(exitButton);
		
		startButton.setBounds(788, 580, 150, 25);
		gameRulesButton.setBounds(788, 610, 150, 25);
		exitButton.setBounds(788, 640, 150, 25);
		
		
		startButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		gameRulesButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		exitButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		startButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame options = new JFrame();
				screen.setEnabled(false);
				new OptionsScreen(options,screen);
			}
		});
		
		gameRulesButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().open(new File("./src/main/resources/GameRules.pdf"));
					} catch(Exception ex) {
						JOptionPane.showMessageDialog(layeredPane, "File not found!");
					}
			}
		});
		exitButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				String[] dialogButtons = {"Yes","No"};
				int reply = JOptionPane.showOptionDialog(layeredPane, "Are you sure?", "Quit Game", 0, JOptionPane.QUESTION_MESSAGE, null, dialogButtons, dialogButtons[1]);
				if(reply==0){
					System.exit(0);
				}
			}	
		});
		
	}
	
}
