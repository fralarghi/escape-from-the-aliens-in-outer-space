package it.polimi.view;

import it.polimi.controller.Controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

public class StartButton extends GameButton{

	JLabel start;
	JLayeredPane layered;
	private Controller controller;
	private boolean activated;
	private GameScreen gameScreen;
	
	StartButton(JLayeredPane layeredPane, Controller controller, GameScreen gameScreen){
		this.gameScreen=gameScreen;
		this.layered=layeredPane;
		this.controller=controller;
		activated=false;
		addButton();
	}
	
	@Override
	public void addButton() {
		start = new JLabel(new ImageIcon("./src/main/resources/start3.png"));
		layered.add(start);
		start.setBounds(200,590,140,85);
		MouseListener startListener = new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if(isInside(e) && activated){
					start.setIcon(new ImageIcon("./src/main/resources/start3.png"));
					activated=false;
					gameScreen.currentClient.printEvents();
					controller.generateMoves();
					controller.printInfo();
					gameScreen.startClicked();
				}
			}
			
			public void mousePressed(MouseEvent e) {
				if(isInside(e) && activated){
					start.setIcon(new ImageIcon("./src/main/resources/start2.png"));
				}
			}
		};
		start.addMouseListener(startListener);
	}

	@Override
	public void activation() {
		this.activated = true;
		start.setIcon(new ImageIcon("./src/main/resources/start.png"));
		controller.notifyYou("Click START to start your turn, "+gameScreen.currentClient+"!");
	}

	@Override
	public void disactivation() {
		this.activated = false;
		start.setIcon(new ImageIcon("./src/main/resources/start3.png"));
	}
	

}
