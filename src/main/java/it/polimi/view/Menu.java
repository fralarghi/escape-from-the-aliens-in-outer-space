package it.polimi.view;


import javax.swing.JFrame;

public class Menu {
	
	public void start(){
		JFrame screen = new JFrame();
		screen.setTitle("Escape from the aliens in outer space");
		screen.setSize(1100,750);
		screen.setResizable(false);
		screen.setVisible(true);
		screen.setLocationRelativeTo(null);
		screen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		new HomeScreen(screen);
		
	}
}