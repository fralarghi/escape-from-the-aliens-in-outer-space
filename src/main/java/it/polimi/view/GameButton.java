package it.polimi.view;

import java.awt.event.MouseEvent;

public abstract class GameButton {

	private static final int BUTTON_WIDTH = 140;
	

	public abstract void addButton();
	
	public abstract void activation();
	
	public abstract void disactivation();

	public boolean isInside(MouseEvent e){
		int x=e.getX();
		int y=e.getY();
		
		float r1=((-11)*x+23*35)/35;
		float r2=((11)*x+62*35)/35;
		float r3=((11)*x-21*35)/35;
		float r4=((-11)*x+106*35)/35;
		
		
		if((x<((GameButton.BUTTON_WIDTH)/2) && 
				((y>Math.round(r1)) &&
				(y<Math.round(r2)))) ||
				(x>((GameButton.BUTTON_WIDTH)/2) &&
				((y>Math.round(r3)) &&
				(y<Math.round(r4))))){
			return true;
		}
		else {
			return false;
		}
	}
	
}
