package it.polimi.view;


import it.polimi.common.observer.BaseObservable;
import it.polimi.common.observer.Event;
import it.polimi.common.observer.Observer;

import it.polimi.view.Console;

public class Client implements Observer {
	
	public String name;
	public String eventsBuffer;
	private String color;
	private Console console;

	public Client(String name) {
		super();
		this.name = name;
		this.eventsBuffer = "";
	}
	
	public Client(String name, Object color, Console console) {
		super();
		this.console = console;
		this.name = name;
		this.color = (String)color;
		this.eventsBuffer = "";
	}

	public void notify(BaseObservable source, Event event) {
		this.eventsBuffer = this.eventsBuffer + BaseObservable.currentObserver + event.getMsg();
	}

	@Override
	public String toString() {
		return name;
	}
	
	public void print(Event event){
		console.write(event.getMsg());
	}
	
	public void printEvents(){
		console.write(eventsBuffer);
		eventsBuffer = "";
	}
	
	public String getColor(){
		return this.color;
	}
	
}
