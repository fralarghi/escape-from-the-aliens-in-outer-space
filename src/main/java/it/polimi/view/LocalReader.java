package it.polimi.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.polimi.controller.Controller;
import it.polimi.model.Player;
import it.polimi.model.Position;
import it.polimi.model.exceptions.*;

public class LocalReader {
	
    public final Controller controller;
    private Scanner scanner;
    public ArrayList<Client> clients;
    Iterator<Client> iterClients;
    int numTurn;
    Client currentClient;
    
    LocalReader(Controller controller, Scanner scanner){
		this.controller = controller;
		this.scanner = scanner;
		clients = new ArrayList<Client>(0);
		numTurn = 1;
		
	}
	
    
	public void start() throws IOException, Exception{
		while(numTurn<40){
			iterClients = clients.iterator();
			round();
			numTurn++;
		}
		throw new WinException("You have completed 39th turn! The game will end!");
	}

	public void round() throws Exception {
		while(iterClients.hasNext()){
    		currentClient = iterClients.next();
    		Player.observing(currentClient);
    		turn();
    	}
	}
	
	public void turn() throws Exception{
		verifyGame();
		if(verifyState()){
			newTurn();
			startPlayer();
			
			requestActivation();
			move();
			requestActivation();
			attack();
		
			passTurn();
		}
	}
	
	private void verifyGame() {
		controller.verifyGame();
	}
	
	public boolean verifyState(){
		return controller.verifyState();
	}
	
	public void newTurn(){
		String choice = "";
		while(!choice.equalsIgnoreCase("start")){
			System.out.println("Insert \"start\" to start your turn "+currentClient+"!");
			choice = scanner.next();
		}
	
		System.out.println("Turn: " + numTurn+"\n");
	}
	
	public void startPlayer(){		
		controller.generateMoves();
		currentClient.printEvents();
		controller.printInfo();
	}

	public void clearConsole(){
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}
	
	public void move() throws Exception {
		final Pattern PATTERN = Pattern.compile("(.),(..)");
        String command = new String();
        
        Matcher m = PATTERN.matcher(command);
        while(!m.matches()){
        	System.out.println("Insert the destination of your move according to this format: \"A,01\"");
        	System.out.println("If you want to surrender insert \"exit\"");
        	
            command = scanner.next();
        	
            m = PATTERN.matcher(command);
            if(m.matches()){
            	try {
                    controller.move(m.group(1), m.group(2));
                } catch (BadPositionException bad){
                    System.err.println(bad.getMessage());
                    move();
                } catch (InsertPositionException ins){
			        m = PATTERN.matcher("");
			        while(!m.matches()){
			        	System.out.println("Insert where you want to make noise according to this format: \"A,01\"");
			            command = scanner.next();
			            m = PATTERN.matcher(command);
			            if(m.matches()){
			            	Position target = new Position(m.group(1).charAt(0), Integer.parseInt(m.group(2)));
			            	controller.notifyNoise(target);
			            }
			        }
			    } catch (TooManyCardsException too){
                	System.err.println(too.getMessage());
                	
                	String choice = "";
                	while(!choice.equals("discard") || !choice.equals("activate")){
                		System.out.println("Insert what you want to do! (discard or activate)");
                		choice = scanner.next();
                		if(choice.equalsIgnoreCase("discard")){
                			boolean discarded = false;
                			while(!discarded){
                				System.out.println("Which card do you want to discard?");
                				choice = scanner.next();
                				discarded = controller.discard(choice);
                			}
                		}
                		else if(choice.equalsIgnoreCase("activate")){
                			cardActivation();
                		}
                	}
                } catch (Exception e){
                	System.err.println("Problem with your move!!");
                } 
            	
            	
            } else {
                if (command.equalsIgnoreCase("exit")) {
                	surrender();
                } else {
                    System.err.println("Unknown command!");
                }
            }
        }
       
		
	}
	
	public void surrender() throws Exception{
		controller.surrender();
    	controller.notifyString(" surrenders!!");
    	verifyGame();
    	passTurn();
    	round();
	}
	
	public void attack(){
		String choice = "";
		if(controller.attackActivated()){
			while(!choice.equalsIgnoreCase("no") && !choice.equalsIgnoreCase("yes")){
				System.out.println("Do you want to attack? (yes or no)");
				choice = scanner.next();
				if(choice.equalsIgnoreCase("yes")){
					controller.attack();
				}
			}
		}
		controller.recover();
		verifyGame();
	}
	
	public void requestActivation(){
		if(controller.canActivate()){
			String answer = "";
			while(controller.cardExist() && !(answer.equalsIgnoreCase("no"))){
				System.out.println("Do you want to activate a card? (yes or no)");
				answer=scanner.next();
				if(answer.equalsIgnoreCase("yes")){
					cardActivation();
				}
				else if(answer.equalsIgnoreCase("no")){
					System.out.println("You will not activate a card right now!");
				}
			}
		}
	}
	
	public void cardActivation(){
		System.out.println("Which card do you want to activate?");
		String answer=scanner.next();
		
		if(answer.equalsIgnoreCase("LIGHTS")){
			final Pattern PATTERN = Pattern.compile("(.),(..)");
	        String command = new String();
	        Matcher m = PATTERN.matcher(command);
	        while(!m.matches()){
	        	System.out.println("Insert where you want to activate it according to this format: \"A,01\"");
	            command = scanner.next();
	            m = PATTERN.matcher(command);
	            if(m.matches()){
	            	try {
	                    controller.activateLights(m.group(1), m.group(2));
						startPlayer();
	                } catch (BadPositionException bad){
	                    System.err.println(bad.getMessage()); 
	                    cardActivation();
	                }
	            }
	            else{
	            	System.err.println("Unknown command!");
	            }
	        }
		}
		
		else {
			try{
				controller.activateCard(answer);
				startPlayer();
			}
			catch(Exception e){
				System.out.println("Invalid choice!!");
				cardActivation();
			}
		}
	}
	
	public void printWinners() {
		controller.refreshPlayers();
		for(Client client: clients){
			System.out.println(client+controller.getResult());
			controller.nextPlayer();
		}
		
	}
	
	public void passTurn() throws IOException{
		System.out.println("End Turn. Press ENTER to pass the turn!");
		System.in.read();
		
		clearConsole();
		System.out.println("Pass the pc to the next player...\n");
		controller.nextPlayer();
		
	}
}

	
	