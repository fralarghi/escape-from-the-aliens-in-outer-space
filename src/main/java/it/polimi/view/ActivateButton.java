package it.polimi.view;

import it.polimi.controller.Controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

public class ActivateButton extends GameButton {

	JLabel activate;
	JLayeredPane layered;
	private boolean activated;
	private GameScreen gameScreen;
	
	ActivateButton(JLayeredPane layeredPane, Controller controller, GameScreen gameScreen){
		this.gameScreen=gameScreen;
		this.layered=layeredPane;
		activated=false;
		addButton();
	}
	
	public void addButton() {
		activate = new JLabel(new ImageIcon("./src/main/resources/activate3.png"));
		layered.add(activate);
		activate.setBounds(345,590,140,85);
		MouseListener activateListener = new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if(isInside(e) && activated){
					activate.setIcon(new ImageIcon("./src/main/resources/activate3.png"));
					activated=false;
					
					JFrame cards = new JFrame();
					gameScreen.screen.setEnabled(false);
					new CardScreen(cards, gameScreen, false);
					
				}
			}
			
			public void mousePressed(MouseEvent e) {
				if(isInside(e) && activated){
					activate.setIcon(new ImageIcon("./src/main/resources/activate2.png"));
				}
			}
		};
		activate.addMouseListener(activateListener);
	}

	@Override
	public void activation() {
		this.activated = true;
		activate.setIcon(new ImageIcon("./src/main/resources/activate.png"));
	}

	@Override
	public void disactivation() {
		this.activated = false;
		activate.setIcon(new ImageIcon("./src/main/resources/activate3.png"));
	}

}
