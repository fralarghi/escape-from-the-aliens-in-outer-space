package it.polimi.view;

import it.polimi.controller.Controller;
import it.polimi.model.ObjectCard;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

public class CardScreen {

	private static final int Y_COORD = 50;
	private JFrame screen;
	private JFrame cards;
	private JLayeredPane layeredPane;
	public String choice;
	private Controller controller;
	private GameScreen gameScreen;
	private boolean discarding;

	public CardScreen(JFrame cards,GameScreen gameScreen,boolean discarding) {
		this.discarding=discarding;
		this.gameScreen=gameScreen;
		this.screen=gameScreen.screen;
		this.cards=cards;
		this.controller=gameScreen.controller;
		layeredPane = new JLayeredPane();
		cards.setVisible(true);
		cards.setSize(750,300);
		cards.setLocationRelativeTo(null);
		cards.setResizable(false);
		cards.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		verifyFrameClosing(screen,cards);
		cards.setTitle("Your cards");
		cards.getContentPane().add(layeredPane);
		layeredPane.setPreferredSize(new Dimension(750,300));
		addCards();
		
		cards.setSize(749,299);
		cards.setSize(750,300);
		
	}
	
	public void drawCard(JLabel card, int x){
		layeredPane.add(card);
		card.setBounds(x, Y_COORD, 151, 139);
	}
	
	public JLabel adrenalineCard(){
		JLabel adrenalineCard = new JLabel(new ImageIcon("./src/main/resources/AdrenalineCard.png"));
		adrenalineCard.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				if(discarding){
					controller.discard("ADRENALINE");
				}
				else {
					controller.activateAdrenaline();
				}
				closeCardScreen(cards);
			}
		});
		return adrenalineCard;
	}
	
	public JLabel defenseCard(){
		JLabel adrenalineCard = new JLabel(new ImageIcon("./src/main/resources/DefenseCard.png"));
		adrenalineCard.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				if(discarding){
					controller.discard("DEFENSE");
				}
				
				closeCardScreen(cards);
			}
		});
		return adrenalineCard;
	}
	
	public JLabel attackCard(){
		JLabel attackCard = new JLabel(new ImageIcon("./src/main/resources/AttackCard.png"));
		attackCard.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				if(discarding){
					controller.discard("ATTACK");
				}
				else{
					controller.activateAttack();
				}
				closeCardScreen(cards);
			}
		});
		return attackCard;
	}
	
	public JLabel sedativesCard(){
		JLabel sedativesCard = new JLabel(new ImageIcon("./src/main/resources/SedativesCard.png"));
		sedativesCard.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				if(discarding){
					controller.discard("SEDATIVES");
				}
				else{
					controller.activateSedatives();
				}
				closeCardScreen(cards);
			}
		});
		return sedativesCard;
	}
	
	public JLabel teleportCard(){
		JLabel teleportCard = new JLabel(new ImageIcon("./src/main/resources/TeleportCard.png"));
		teleportCard.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				if(discarding){
					controller.discard("TELEPORT");
				}
				else{
					controller.activateTeleport();
				}
				closeCardScreen(cards);
			}
		});
		return teleportCard;
	}
	
	public JLabel lightsCard(){
		JLabel lightsCard = new JLabel(new ImageIcon("./src/main/resources/SpotlightCard.png"));
		lightsCard.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				if(discarding){
					controller.discard("LIGHTS");
				}
				else{
					gameScreen.setLightsActivated();
				}
				closeCardScreen(cards);
			}
		});
		return lightsCard;
	}
	
	public void addCards(){
		
		int xCard=50;
		
		for(ObjectCard card : controller.objectCards()){
			switch(card){
			case ADRENALINE: drawCard(adrenalineCard(),xCard); break;
			case ATTACK: drawCard(attackCard(),xCard); break;
			case LIGHTS: drawCard(lightsCard(),xCard); break;
			case SEDATIVES: drawCard(sedativesCard(),xCard); break;
			case TELEPORT: drawCard(teleportCard(),xCard); break;
			case DEFENSE: drawCard(defenseCard(),xCard); break;
			default: break;
			}
			xCard=xCard+155;
		}
		
	}
	
	
	public void verifyFrameClosing(final JFrame screen, final JFrame cards){
		cards.addWindowListener(new WindowAdapter() {
			public void windowClosing (WindowEvent e) {
				cards.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				screen.setEnabled(true);
				gameScreen.cardActivated();
			}
		});
	}
	
	public void closeCardScreen(JFrame cards){
		screen.setEnabled(false);
		WindowEvent close = new WindowEvent(cards, WindowEvent.WINDOW_CLOSING);
		cards.dispatchEvent(close);
		gameScreen.cardActivated();
	}
	
	
	
}