package it.polimi.view;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

import it.polimi.controller.Controller;
import it.polimi.model.exceptions.WinException;

public class AttackButton extends GameButton{

	JLabel attack;
	JLayeredPane layered;
	private boolean activated;
	private Controller controller;
	private GameScreen gameScreen;
	
	AttackButton(JLayeredPane layeredPane, Controller controller, GameScreen gameScreen){
		this.gameScreen=gameScreen;
		this.layered=layeredPane;
		this.controller=controller;
		activated=false;
		addButton();
	}
	
	@Override
	public void addButton() {
		attack = new JLabel(new ImageIcon("./src/main/resources/attack3.png"));
		layered.add(attack);
		attack.setBounds(490,590,140,85);
		MouseListener attackListener = new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if(isInside(e) && activated){
					attack.setIcon(new ImageIcon("./src/main/resources/attack3.png"));
					activated=false;
					try{
						controller.attack();
					}
					catch(WinException win){
						gameScreen.endGame(win.getMessage());
					}
					
				}
			}
			
			public void mousePressed(MouseEvent e) {
				if(isInside(e) && activated){
					attack.setIcon(new ImageIcon("./src/main/resources/attack2.png"));
				}
			}
		};
		attack.addMouseListener(attackListener);
	}

	@Override
	public void activation() {
		this.activated = true;
		attack.setIcon(new ImageIcon("./src/main/resources/attack.png"));
	}

	@Override
	public void disactivation() {
		this.activated = false;
		attack.setIcon(new ImageIcon("./src/main/resources/attack3.png"));
	}
}
