package it.polimi.view;

import java.awt.Color;
import java.awt.Cursor;

import javax.swing.BorderFactory;
import javax.swing.JLayeredPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

public class Console {
	
	JTextArea console = new JTextArea();
	JScrollPane panel = new JScrollPane(console);
	
	public Console(JLayeredPane layeredPane) {
		drawPane(layeredPane);
		drawConsole();
	}
	
	public void drawPane(JLayeredPane layeredPane){
		layeredPane.add(panel, new Integer(0));
		panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black),"CONSOLE",TitledBorder.CENTER,TitledBorder.TOP));
		panel.setBackground(Color.white);
		panel.setBounds(800, 40, 250, 520);
		panel.setOpaque(true);
	}
	
	public void drawConsole(){
		console.setBackground(Color.white);
		console.setBounds(810, 60, 230, 480);
		console.setOpaque(true);
		console.setLineWrap(true);
		console.setEditable(false);
		console.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
	}
	
	public void write(String text){
		console.append(text+"\n");
	}
	
	public void clear(){
		console.setText("");
	}
	
}
