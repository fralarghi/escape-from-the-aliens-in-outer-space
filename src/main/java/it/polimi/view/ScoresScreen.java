package it.polimi.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

public class ScoresScreen {

	private List<String> results;
	private ArrayList<Client> clients;
	JLayeredPane layeredPane;
	private String endCondition;

	public ScoresScreen(JFrame scores, List<String> results, ArrayList<Client> clients, String endCondition) {
		this.endCondition=endCondition;
		this.clients=clients;
		this.results=results;
		layeredPane = new JLayeredPane();
		scores.setVisible(true);
		scores.setSize(400,600);
		scores.setLocationRelativeTo(null);
		scores.setResizable(false);
		scores.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		scores.setTitle("SCORES");
		scores.getContentPane().add(layeredPane);
		layeredPane.setPreferredSize(new Dimension(400,600));
		
		writeResults();
		drawExitButton();

		scores.setSize(399,599);
		scores.setSize(400,600);
	}
	
	private void drawExitButton() {
		JButton exitButton = new JButton("Exit Game");
		layeredPane.add(exitButton);
		exitButton.setBounds(150, 500, 100, 40);
		exitButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
				
			}
		});
	}

	private void writeResults(){
		int i = 0;
		int yResult=80;
		JLabel condition = new JLabel(endCondition);
		layeredPane.add(condition);
		condition.setBounds(50, 25, 300, 25);
		
		for(String string: results){
			string = clients.get(i).toString() + string;
			
			JLabel result = new JLabel(string);
			layeredPane.add(result);
			result.setBounds(50, yResult, 300, 25);
			
			yResult=yResult+40;
			i++;
		}
		
	}
}
