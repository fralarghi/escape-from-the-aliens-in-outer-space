package it.polimi.view;

import it.polimi.controller.Controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

public class PassButton extends GameButton{
	
	JLabel pass;
	JLayeredPane layered;
	private Controller controller;
	private boolean activated;
	private GameScreen gameScreen;
	
	PassButton(JLayeredPane layeredPane, Controller controller, GameScreen gameScreen){
		this.gameScreen = gameScreen;
		this.layered=layeredPane;
		this.controller=controller;
		activated=false;
		addButton();
	}
	
	
	@Override
	public void addButton() {
		pass = new JLabel(new ImageIcon("./src/main/resources/pass3.png"));
		layered.add(pass);
		pass.setBounds(635,590,140,85);
		MouseListener passListener = new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if(isInside(e) && activated){
					pass.setIcon(new ImageIcon("./src/main/resources/pass3.png"));
					activated=false;
					gameScreen.console.clear();
					controller.notifyYou("Pass the PC to the next player...");
					controller.recover();
					gameScreen.endTurn();
				}
			}
			
			public void mousePressed(MouseEvent e) {
				if(isInside(e) && activated){
					pass.setIcon(new ImageIcon("./src/main/resources/pass2.png"));
				}
			}
		};
		pass.addMouseListener(passListener);
	}


	@Override
	public void activation() {
		this.activated = true;
		pass.setIcon(new ImageIcon("./src/main/resources/pass.png"));
	}


	@Override
	public void disactivation() {
		this.activated = false;
		pass.setIcon(new ImageIcon("./src/main/resources/pass3.png"));
	}

}
