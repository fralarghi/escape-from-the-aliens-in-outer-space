package it.polimi.controller;


import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

import it.polimi.model.Alien;
import it.polimi.model.Board;
import it.polimi.model.ObjectCard;
import it.polimi.model.Player;
import it.polimi.model.Position;
import it.polimi.model.Sector;
import it.polimi.model.State;
import it.polimi.model.exceptions.BadPositionException;
import it.polimi.model.exceptions.InsertPositionException;
import it.polimi.model.exceptions.WinException;
import it.polimi.model.Human;


public class Controller {
	
	private final Board board;
	private Player currentPlayer;
	
	public Controller (Board board){
		this.board = board;
		nextPlayer();
	}

	
	public void nextPlayer(){
		if(board.iterPlayers.hasNext()){
			currentPlayer = board.iterPlayers.next();
		}
		else{ 
			refreshPlayers();
		}
	}
	
	public void refreshPlayers(){
		board.iterPlayers = board.players.iterator();
		currentPlayer = board.iterPlayers.next();
	}
	

	public void move(Position pos) throws InsertPositionException {
		Position destination = pos;
		Sector sector;
		
		if(currentPlayer.validMoves.contains(destination)){
			currentPlayer.move(destination);
			sector = board.map.get(destination);
			switch (sector) {
				case SAFE : 
					notifyYou("You are in a safe position! No one has heard anything..");
					notifyString(": no one has heard anything..");
					break;
				case DANGEROUS :
					if(!currentPlayer.isSedated()){
						currentPlayer.drawSectorCard(board);
					}
					else{
						notifyYou("You are sedated! No one has heard anything and you are not in danger..");
					}
					break;
				case LIFEBOAT : 
					if(sector.isUsable()){
						currentPlayer.drawLifeboatCard(board, sector);
					}
					break;
				default: notifyYou("Something go wrong!!");
					break;
			}
			verifyGame();
		}
		else throw new BadPositionException("Invalid destination inserted!");
	}

	public void surrend() {
		currentPlayer.eliminated();
	}
	
	public boolean isAlive() {
		if(currentPlayer.getStat() == State.ALIVE){
			return true;
		}
		else return false;
	}
	
	public void printInfo() {
		currentPlayer.printInfo();
	}

	public void generateMoves() {
		currentPlayer.restartValidMoves();
		currentPlayer.validMoves.add(currentPlayer.getPos());
		for(int i=0; i < currentPlayer.getMoveRange(); i++){
			try{
			board.addValidPositions(currentPlayer.validMoves);
			currentPlayer.refreshMoves();
			}
			catch(Exception e){
				notifyYou("Error in adding valid moves");
			}
		}
	}

	public boolean cardExist() {
		if(!currentPlayer.objectCards.isEmpty()){
			return true;
		}
		else return false;
	}

	public void activateLights(Position pos) {
		((Human)currentPlayer).activateLights(board, pos);
	}

	public void attack() {
		notifyYou("You have attacked!");
		notifyString(": attacks in the position "+currentPlayer.getPos());
		currentPlayer.attack(board.players);
		recover();
		verifyGame();
	}
	
	public void recover() {
		currentPlayer.recover();
	}

	public void verifyGame() {
		int humans=0;
		int deadHumans=0;
		int aliens=0;
		int deadAliens=0;
		int lastDeadHumans=0;
		boolean lastHuman = false;
		for(Player player : board.players){
			if(player instanceof Human && player.getStat()!=State.WINNER){
				humans++;
				if(player.getStat()==State.DEAD){
					deadHumans++;
				}
			}
			else {
				aliens++;
				if(player.getStat()==State.DEAD){
					deadAliens++;
				}
			}
		}
		
		if(!lastHuman){
			if(humans==deadHumans){
				for(Player player : board.players){
					if(player instanceof Alien){
						if(player.getStat()==State.ALIVE){
							player.setStat(State.WINNER);
						}
					}
				}
				throw new WinException("All reamaining humans are dead! The Aliens win!");
			}
			else if(humans-deadHumans==1){
				lastHuman=true;
				lastDeadHumans = deadHumans;
			}
		
			if(deadAliens==aliens){
				for(Player player : board.players){
					if(player instanceof Human){
						if(player.getStat()==State.ALIVE){
							player.setStat(State.WINNER);
						}
					}
				}
				throw new WinException("There are no more aliens in the game! The remaining Humans win");
			}
		}
		else{
			if(deadHumans>lastDeadHumans){
				for(Player player : board.players){
					if(player instanceof Alien){
						if(player.getStat()==State.ALIVE){
							player.setStat(State.WINNER);
						}
					}
				}
				throw new WinException("All reamaining humans are dead! The remaining Aliens win!");
			}
			else if(humans==0){
				for(Player player : board.players){
					if(player instanceof Alien){
						player.setStat(State.LOSER);
					}
				}
				throw new WinException("The last human saves himself! The Aliens lose!");
			}
		}
	}
	
	public List<String> getResult() {
		List<String> results = new CopyOnWriteArrayList<String>();
		for(Player player: board.players){
			results.add(player.getResults());
		}
		return results;
	}

	public boolean discard(String choice) {
		return currentPlayer.removeObjectCard(choice.toUpperCase());
	}

	public boolean attackActivated() {
		return currentPlayer.attackActivated();
	}
	
	public boolean canActivate() {
		if(currentPlayer.toString().equalsIgnoreCase("human")){
			return true;
		}
		else return false;
	}
	
	public void notifyNoise(Position pos) {
		currentPlayer.notifyNoise(pos);
		
	}

	public void notifyString(String string) {
		currentPlayer.notifyString(string);
		
	}
	
	public void notifyYou(String string) {
		currentPlayer.notifyYou(string);

	}


	public CopyOnWriteArraySet<Position> validMoves() {
		return currentPlayer.validMoves;
	}


	public Position getPlayerPos() {
		return currentPlayer.getPos();
		
	}


	public String getType() {
		return currentPlayer.toString();
		
	}
	


	public List<ObjectCard> objectCards() {
		return currentPlayer.objectCards;
	}


	public void activateAdrenaline() {
		((Human)currentPlayer).activateAdrenaline();
	}
	
	public void activateAttack() {
		((Human)currentPlayer).activateAttack();
	}
	
	public void activateSedatives() {
		((Human)currentPlayer).activateSedatives();
	}
	
	public void activateTeleport() {
		((Human)currentPlayer).teleport();
	}
	


	public String sectorIn(Position pos) {
		return board.sectorIn(pos);
	}

}