package it.polimi.model;

import it.polimi.common.observer.Event;
import it.polimi.view.Sounds;

import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;

public class Human extends Player {
	
	
	private boolean attackActivated;

	//COSTRUTTORE
	
	Human () {
		this.stat = State.ALIVE;
		this.pos = new Position('L',8);
		this.moveRange = 1;
		
		this.sedated = false;
		this.attackActivated=false;
		
		this.objectCards = new ArrayList<ObjectCard>(0);
		this.validMoves = new CopyOnWriteArraySet<Position>();
		this.validMoves.add(this.pos);
		
		inizializeLifeboats();
	}
	
	//METODI
	
	public void teleport(){
		this.pos = new Position('L', 8);
		notify(new Event("You have been teleported to the starting position L,8 !!"));
		removeObjectCard("TELEPORT");
		Sounds.teleport();
	}	
	
	public void activateSedatives(){
		this.sedated = true;
		notify(new Event("Sedatives activeted! You will not draw the Sector Card this turn!"));
		removeObjectCard("SEDATIVES");
		Sounds.sedatives();
	}
	
	public void activateAdrenaline(){
		this.moveRange++;
		notify(new Event("Adrenaline activated! You can move by 2 cells this turn!"));
		removeObjectCard("ADRENALINE");
		Sounds.adrenaline();
	}
	
	public boolean verifyDefense(){
		if(verifyObjectCard("DEFENSE")){
			notify(new Event("Defense activated! Someone have avoided the attack!"));
			removeObjectCard("DEFENSE");
			return true;
		}
		else {
	
			return false;		
		}
	}
	
	public void activateAttack(){
		this.attackActivated=true;
		notify(new Event("Attack activated! You can attack this turn!"));
		removeObjectCard("ATTACK");
		Sounds.attackActivated();
	}
	
	public void recover(){
		this.sedated = false;
		this.attackActivated = false;
		this.moveRange = 1;
	}
	
	@Override
	public String toString() {
		return "Human";
	}
	
	public void refreshMoves() {
		validMoves.remove(pos);
	}
	
	public boolean attackActivated(){
		return attackActivated;
	}

	public void activateLights(Board board, Position coordinates) {
		Sounds.lights();
		notify(new Event("You have activated lights in " +coordinates));
		CopyOnWriteArraySet<Position> enlightedPositions = new CopyOnWriteArraySet<Position>();
		enlightedPositions.add(coordinates);
		board.addValidPositions(enlightedPositions);
		for(Player player : board.players){
			if(enlightedPositions.contains(player.getPos())){
				notify(new Event("A player is enlighted in "+player.getPos()+" !!"));
				notifyAll(new Event(": A player is enlighted in "+player.getPos()+" !!"));
			}
		}
		removeObjectCard("LIGHTS");
	}
	
	public String getResults() {
		if(getStat()==State.ALIVE){
			setStat(State.LOSER);
		}
		return "        " +toString() + "        " + getStat() + "\n";
	}
	
}
