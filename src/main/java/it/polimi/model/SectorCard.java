package it.polimi.model;

public enum SectorCard {
	NOISE, NOISE_SOMEWHERE, SILENCE, NOISE_AND_DRAW, NOISE_SOMEWHERE_AND_DRAW, SILENCE_AND_DRAW;
	
}
