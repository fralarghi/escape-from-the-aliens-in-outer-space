package it.polimi.model;

import java.util.Random;


public class Position {
	
	private static final int NUMBER_OF_COLUMNS = 23;
	private static final int NUMBER_OF_ROWS = 14;
	private final Character col;
	private final Integer row;
	
	
	public Position(){
		Random r = new Random();
		this.col = (char)(r.nextInt(NUMBER_OF_COLUMNS) + 'A');
		this.row = r.nextInt(NUMBER_OF_ROWS)+1;
	}
	
	public Position(Character col, Integer row) {
		this.col = col;
		this.row = row;
	}

	public Character getCol() {
		return col;
	}

	public Integer getRow() {
		return row;
	}

	public boolean evenColumn(){
		if((this.getCol()%2)==0){
			return true;
		}
		else return false;
	}
	
	public boolean exists() {
		if((this.col >= 'A' && this.col <= 'W') && (this.row >= 1 && this.row <= 14)){
			return true;
		}
		else return false;
	}
		
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((col == null) ? 0 : col.hashCode());
		result = prime * result + ((row == null) ? 0 : row.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (col == null) {
			if (other.col != null)
				return false;
		} else if (!col.equals(other.col))
			return false;
		if (row == null) {
			if (other.row != null)
				return false;
		} else if (!row.equals(other.row))
			return false;
		return true;
	}

	@Override
    public String toString() {
        return "(" +
                 col +
                ", " + row +
                ')';
    }
	
	

}
