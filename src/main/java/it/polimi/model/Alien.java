package it.polimi.model;

import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;

public class Alien extends Player {
	
	//COSTRUTTORE

	public Alien () {
		this.stat = State.ALIVE;
		this.pos = new Position('L',6);
		this.moveRange = 2;
		this.sedated = false;
		
		this.objectCards = new ArrayList<ObjectCard>(0);
		this.validMoves = new CopyOnWriteArraySet<Position>();
		this.validMoves.add(this.pos);
		
		inizializeLifeboats();
	}

	//METODI
	
	public boolean verifyDefense(){
		return false;
	}

	protected void feeding(){
		this.moveRange++;
	}

	
	
	@Override
	public String toString() {
		return "Alien";
	}
	
	public void refreshMoves() {
		validMoves.remove(pos);
		for(Position position: lifeboatPos){
			if(validMoves.contains(position)){
				validMoves.remove(position);
			}
		}
	}
	
	public boolean attackActivated() {
		return true;
	}
	
	public void recover(){
		
	}

	public String getResults() {
		if(getStat()==State.ALIVE){
			setStat(State.WINNER);
		}
		return "        " +toString() + "        " + getStat() + "\n";
	}
	
	
}


	