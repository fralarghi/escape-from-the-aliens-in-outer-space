package it.polimi.model;

import it.polimi.common.observer.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;

public class Board extends BaseObservable {
	
	//NUMERO DI CARTE NEL GIOCO
	
	private static final int NOISE_CARDS = 5;
	private static final int NOISE_SOMEWHERE_CARDS = 4;
	private static final int SILENCE_CARDS = 4;
	private static final int NOISE_AND_DRAW_CARDS = 4;
	private static final int NOISE_SOMEWHERE_AND_DRAW_CARDS = 4;
	private static final int SILENCE_AND_DRAW_CARDS = 4;
	
	private static final int GREEN_CARDS = 4;
	private static final int RED_CARDS = 2;
	
	private static final int ATTACK_CARDS = 2;
	private static final int ADRENALINE_CARDS = 2;
	private static final int DEFENSE_CARDS = 2;
	private static final int LIGHTS_CARDS = 2;
	private static final int SEDATIVES_CARDS = 2;
	private static final int TELEPORT_CARDS = 2;
	
	private static final int NUMBER_OF_SECTORS = 322;
	
	
	
	
	//INIZIALIZZAZIONI
	
	public ArrayList<Player> players;
	public Iterator<Player> iterPlayers;
	public ArrayList<SectorCard> sectorDeck;
	public ArrayList<LifeboatCard> lifeboatDeck;
	ArrayList<ObjectCard> objectCardDeck;
	
	public HashMap<Position, Sector> map;
	
	
	//COSTRUTTORE
	
	public Board(){
		inizializePlayers(4);
		
		inizializeSectorDeck();
		inizializeLifeboatDeck();
		inizializeObjectCardDeck();
		
		inizializeGalilei();
	}
	
	public Board(int choice){
		inizializePlayers(choice);
		inizializeSectorDeck();
		inizializeLifeboatDeck();
		inizializeObjectCardDeck();
		
		inizializeGalilei();
	}
	
	private void inizializePlayers(int numOfPlayers){
		players = new ArrayList<Player>(0);
		
		for(int i=0; i<numOfPlayers; i++){
			if(i%2==0){
				players.add(new Alien());
			}
			else players.add(new Human());
		}
		Collections.shuffle(players);
		iterPlayers = players.iterator();
	}
	
	private void inizializeSectorDeck(){
		sectorDeck = new ArrayList<SectorCard>(0);
		
		for(int i=0; i<NOISE_CARDS; i++){
			sectorDeck.add(SectorCard.NOISE);
		}
		for(int i=0; i<NOISE_SOMEWHERE_CARDS; i++){
			sectorDeck.add(SectorCard.NOISE_SOMEWHERE);
		}
		for(int i=0; i<SILENCE_CARDS; i++){
			sectorDeck.add(SectorCard.SILENCE);
		}
		for(int i=0; i<NOISE_AND_DRAW_CARDS; i++){
			sectorDeck.add(SectorCard.NOISE_AND_DRAW);
		}
		for(int i=0; i<NOISE_SOMEWHERE_AND_DRAW_CARDS; i++){
			sectorDeck.add(SectorCard.NOISE_SOMEWHERE_AND_DRAW);
		}
		for(int i=0; i<SILENCE_AND_DRAW_CARDS; i++){
			sectorDeck.add(SectorCard.SILENCE_AND_DRAW);
		}
		
		Collections.shuffle(sectorDeck);
	}
	
	private void inizializeLifeboatDeck(){
		lifeboatDeck = new ArrayList<LifeboatCard>(0);
		
		for(int i=0; i<GREEN_CARDS; i++){
			lifeboatDeck.add(LifeboatCard.GREEN);
		}
		for(int i=0; i<RED_CARDS; i++){
			lifeboatDeck.add(LifeboatCard.RED);
		}
		
		Collections.shuffle(lifeboatDeck);

	}
	
	private void inizializeObjectCardDeck(){
		objectCardDeck = new ArrayList<ObjectCard>(0);
		
		for(int i=0; i<ADRENALINE_CARDS; i++){
			objectCardDeck.add(ObjectCard.ADRENALINE);
		}
		for(int i=0; i<ATTACK_CARDS; i++){
			objectCardDeck.add(ObjectCard.ATTACK);
		}
		for(int i=0; i<DEFENSE_CARDS; i++){
			objectCardDeck.add(ObjectCard.DEFENSE);
		}
		for(int i=0; i<LIGHTS_CARDS; i++){
			objectCardDeck.add(ObjectCard.LIGHTS);
		}
		for(int i=0; i<SEDATIVES_CARDS; i++){
			objectCardDeck.add(ObjectCard.SEDATIVES);
		}
		for(int i=0; i<TELEPORT_CARDS; i++){
			objectCardDeck.add(ObjectCard.TELEPORT);
		}
		
		Collections.shuffle(objectCardDeck);

	}
	
	public void inizializeGalilei(){
		map = new HashMap<Position, Sector>(0);
		String fileLine = new String();
		String tempString = new String();
		
		
		try{			
			BufferedReader reader = new BufferedReader(new FileReader("C:/Users/Francesco/git/Progetto/src/main/java/it/polimi/model/Galilei.txt"));
			for(int i=0; i<NUMBER_OF_SECTORS; i++){
				fileLine = reader.readLine();
			
				Character col = fileLine.charAt(0);
				String integer = fileLine.substring(2,4);
				int row = Integer.parseInt(integer);
				tempString = fileLine.substring(5);
			
				Position position = new Position(col, row);
				Sector sector = Sector.valueOf(tempString);
			
				map.put(position, sector);
			}
			reader.close();	
		}
		catch(Exception e){
			
		}
		
	}
	
	//METODI
	
	
	private void insertNearPosition(Position nearPosition, CopyOnWriteArraySet<Position> listToAdd){	
		if(!listToAdd.contains(nearPosition)){
			if(nearPosition.exists()){
				if((map.get(nearPosition)).isValidSector()){
					listToAdd.add(nearPosition);
				}
			}
		}
	}
	
	public void addValidPositions(CopyOnWriteArraySet<Position> listToAdd){
		
		CopyOnWriteArraySet<Position> startingValidPositions = listToAdd;
		
		for(Position position : startingValidPositions){
			if(position.evenColumn()){
				insertNearPosition(new Position(position.getCol(), position.getRow()-1), listToAdd);
				insertNearPosition(new Position(position.getCol(), position.getRow()+1), listToAdd);
				insertNearPosition(new Position((char)(position.getCol()-1), position.getRow()+1), listToAdd);
				insertNearPosition(new Position((char)(position.getCol()-1), position.getRow()), listToAdd);
				insertNearPosition(new Position((char)(position.getCol()+1), position.getRow()+1), listToAdd);
				insertNearPosition(new Position((char)(position.getCol()+1), position.getRow()), listToAdd);		
			}
			
			else {
				insertNearPosition(new Position(position.getCol(), position.getRow()-1), listToAdd);
				insertNearPosition(new Position(position.getCol(), position.getRow()+1), listToAdd);
				insertNearPosition(new Position((char)(position.getCol()-1), position.getRow()-1), listToAdd);
				insertNearPosition(new Position((char)(position.getCol()-1), position.getRow()), listToAdd);
				insertNearPosition(new Position((char)(position.getCol()+1), position.getRow()-1), listToAdd);
				insertNearPosition(new Position((char)(position.getCol()+1), position.getRow()), listToAdd);
			}	
		}
		
	}

	public String sectorIn(Position pos) {
		return map.get(pos).name();
	}

	
	
}
	

