package it.polimi.model;

public enum Sector {
	SAFE, DANGEROUS, ALIEN, HUMAN, VOID, LIFEBOAT;
	
	public boolean isValidSector(){
		if(this!=Sector.VOID && this!=Sector.ALIEN && this!=Sector.HUMAN){
			return true;
		}
		else return false;
	}
	
	public boolean usable;
	
	private Sector() {
		this.usable = true;
	}

	public void use(){
		this.usable = false;
	}

	public boolean isUsable() {
		return usable;
	}	
}
