package it.polimi.model;

import it.polimi.common.observer.BaseObservable;
import it.polimi.common.observer.Event;
import it.polimi.model.exceptions.GreenLifeboatException;
import it.polimi.model.exceptions.InsertPositionException;
import it.polimi.model.exceptions.RedLifeboatException;
import it.polimi.model.exceptions.TooManyCardsException;

import java.util.*;
import it.polimi.view.Sounds;
import java.util.concurrent.CopyOnWriteArraySet;

public abstract class Player extends BaseObservable {

	protected Position pos;
	protected State stat;
	protected boolean haveAttacked;
	protected int moveRange;
	protected boolean sedated;
	
	protected List<Position> lifeboatPos;
	public List<ObjectCard> objectCards;
	public CopyOnWriteArraySet<Position> validMoves;
	
	//GETTER E SETTER
	
	public Position getPos(){
		return this.pos;
	}
	
	public int getMoveRange(){
		return moveRange;
	}
	
	public void setStat(State stato){
		this.stat = stato;
	}
	
	public State getStat(){
		return this.stat;
	}

	
	public boolean isSedated() {
		return sedated;
	}
	
	//METODI 
	
	public void printInfo(){
		notify(new Event("Available objects=" + objectCards));
	}
	
	protected void inizializeLifeboats(){
		this.lifeboatPos = new ArrayList<Position>(0);
		this.lifeboatPos.add(new Position('B',2));
		this.lifeboatPos.add(new Position('B',13));
		this.lifeboatPos.add(new Position('V',2));
		this.lifeboatPos.add(new Position('V',13));
	}
	
	protected void drawObjectCard(ArrayList<ObjectCard> objectCardDeck){
		ObjectCard card = objectCardDeck.get(0);
		this.objectCards.add(card);
		objectCardDeck.remove(0);
		notify(new Event("You draw the card "+card));
		if(this.objectCards.size()>4){
			throw new TooManyCardsException("You have more than 4 cards. You have to activate a card or discard one!");
		}
	}
	
	protected boolean samePosition(Player attackedPlayer){
		if((attackedPlayer.pos).equals(this.pos)){
			return true;
		}
		else return false;
	}
	
	public void eliminated(){
		this.setStat(State.DEAD);
		this.validMoves.clear();
		this.objectCards.clear();
	}
	
	public void attack(ArrayList<Player> players){
		for(Player attackedPlayer : players){
			if(!(this.equals(attackedPlayer))){
				if(samePosition(attackedPlayer) && attackedPlayer.getStat()==State.ALIVE){
					if(attackedPlayer instanceof Human){
						if(attackedPlayer.verifyDefense()){
							notifyAll(new Event("Someone avoids the attack thanks to the card DEFENSE"));
							Sounds.attack();
						}
						else{
							attackedPlayer.eliminated();
							notifyAll(new Event(": Someone was killed"));
							notify(new Event("You have killed an Human!"));
							Sounds.humanKilled();
							if(this instanceof Alien){
								((Alien)this).feeding();
							}
						}
					}
					else{
						attackedPlayer.eliminated();
						notifyAll(new Event(": Someone was killed"));
						notify(new Event("You have killed an Alien!!"));
						Sounds.alienKilled();
					}
						
				}
			}
			else{
				Sounds.attack();
			}
		}
	}
	
	
	public void move(Position destination){
		this.pos = destination;
		notify(new Event("Your current position is now "+destination+" !!"));
		restartValidMoves();
	}
	
	public void restartValidMoves(){
		validMoves.clear();
	}
	
	public boolean removeObjectCard (String cardChoice){
		for(ObjectCard card : objectCards){
			if(card==(ObjectCard.valueOf(cardChoice))){
				objectCards.remove(card);
				return true;
			}
		}
		return false;
	}

	public void drawLifeboatCard(Board board, Sector sector) {
		LifeboatCard random = board.lifeboatDeck.get(0);
		board.lifeboatDeck.remove(0);
		sector.use();
		
		switch (random){
			case RED : 
				notifyYou("This lifeboat is broken and out of order!!");
				notifyAll(new Event(": the lifeboat in position "+pos+" is broken and out of order!!"));
				throw new RedLifeboatException("");
			
			case GREEN : 
				stat=State.WINNER;
				notifyYou("You saved your ass! You made it!");
				notifyAll(new Event(" takes the lifeboat in position "+pos+" and escapes from the aliens!"));
				throw new GreenLifeboatException("");		
		}
	}
	
	public void drawSectorCard(Board board){
		SectorCard card;
		card = board.sectorDeck.get(0);
		board.sectorDeck.remove(0);
		switch (card){
			case NOISE : 
				notifyYou("You make a noise in "+this.pos);
				notifyAll(new Event(" makes noise in "+this.pos));
				Sounds.noise();
				break;
			case NOISE_SOMEWHERE : 
				throw new InsertPositionException("");
			case SILENCE : 
				notifyYou("You don't make any noise! No one has heard anything..");
				notifyAll(new Event(": no one has heard anything.."));
				break;
			case NOISE_AND_DRAW : 	
				notifyYou("You make a noise in "+this.pos);
				notifyAll(new Event(" makes noise in "+this.pos));
				drawObjectCard(board.objectCardDeck);
				Sounds.noise();
				break;
			case NOISE_SOMEWHERE_AND_DRAW : 
				drawObjectCard(board.objectCardDeck);
				throw new InsertPositionException("");
			case SILENCE_AND_DRAW : 
				notifyYou("You don't make any noise! No one has heard anything..");
				notifyAll(new Event(": no one has heard anything.."));
				drawObjectCard(board.objectCardDeck);
				break;
		}
		
	}
	
	public boolean verifyObjectCard(String cardChoice){
		if(objectCards.contains(ObjectCard.valueOf(cardChoice))){
			notify(new Event("The card "+cardChoice+" is removed!"));
			return true;
		}
		else{
			return false;
		}
		
	}

	public abstract void refreshMoves();
	
	protected abstract boolean verifyDefense();
	
	public abstract boolean attackActivated();

	public abstract void recover();

	public void notifyNoise(Position pos) {
		notifyAll(new Event(" makes noise in "+pos));
		
	}

	public void notifyString(String string) {
		notifyAll(new Event(string));
		
	}

	public void notifyYou(String string) {
		notify(new Event(string));
		
	}

	public abstract String getResults();
}
