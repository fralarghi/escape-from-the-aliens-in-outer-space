package it.polimi.model.exceptions;

@SuppressWarnings("serial")
public class BadPositionException extends RuntimeException{
	public BadPositionException(String message) {
        super(message);
    }
}
