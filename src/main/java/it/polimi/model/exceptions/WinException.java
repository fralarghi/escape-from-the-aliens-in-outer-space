package it.polimi.model.exceptions;

@SuppressWarnings("serial")
public class WinException extends RuntimeException{
	 public WinException(String message) {
	        super(message);
	    }
}
