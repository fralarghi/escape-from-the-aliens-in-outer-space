package it.polimi.model.exceptions;

public class RedLifeboatException extends RuntimeException {
	public RedLifeboatException(String message) {
        super(message);
    }
}
