package it.polimi.model.exceptions;

@SuppressWarnings("serial")
public class TooManyCardsException extends RuntimeException{
	 public TooManyCardsException(String message) {
	        super(message);
	    }
}
