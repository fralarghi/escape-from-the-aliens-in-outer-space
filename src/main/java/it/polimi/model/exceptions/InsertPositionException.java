package it.polimi.model.exceptions;

@SuppressWarnings("serial")
public class InsertPositionException extends RuntimeException{
	public InsertPositionException(String message) {
        super(message);
    }
}
