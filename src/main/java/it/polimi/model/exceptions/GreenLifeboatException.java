package it.polimi.model.exceptions;

public class GreenLifeboatException extends RuntimeException {
	public GreenLifeboatException(String message) {
        super(message);
    }
}
