package it.polimi.model;

public enum State {
	ALIVE, DEAD, WINNER, LOSER;
	
}
